<?php

use Illuminate\Database\Seeder;
use App\Entities\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'cpf'        =>  '11122233346',
            'name'       =>  'Pedro da Costa',
            'phone'      =>  '9299998888',
            'birth'      =>  '1982-11-15',
            'gender'     =>  'M',
            'email'      =>  'ricardo.souza1910@hotmail.com.br',
            'password'   =>   env('PASSWORD_HASH') ? bcrypt('123456') : '123456',

        ]);
    }
}
