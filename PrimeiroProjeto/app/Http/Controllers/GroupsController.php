<?php

namespace App\Http\Controllers;

use App\Http\Requests\GroupCreateRequest;
use App\Http\Requests\GroupUpdateRequest;
use App\Repositories\GroupRepository;
use App\Repositories\InstituitionRepository;
use App\Repositories\USerRepository;
use App\Services\GroupService;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class GroupsController.
 *
 * @package namespace App\Http\Controllers;
 */
class GroupsController extends Controller
{
    protected $service;
    protected $repository;
    protected $userRepository;
    protected $instituitionRepository;

    public function __construct(GroupRepository $repository, GroupService $service, UserRepository $userRepository, InstituitionRepository $instituitionRepository)
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->userRepository = $userRepository;
        $this->instituitionRepository = $instituitionRepository;
    }

    public function index()
    {
        $groups = $this->repository->all();
        $user_list = $this->userRepository->selectBoxList();
        $instituition_list = $this->instituitionRepository->selectBoxList();
        
        return view('group.index', [
            'groups' => $groups,
            'user_list' => $user_list,
            'instituition_list' => $instituition_list,
        ]);
    }

    public function store(GroupCreateRequest $request)
    {
        $request = $this->service->store($request->all());

        session()->flash('success', [
            'success' => $request['success'],
            'message' => $request['message'],
        ]);

        return redirect()->route('group.index');
    }

    public function show($id)
    {

    }

    public function edit($id)
    {

    }

    public function update(GroupUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $group = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Group updated.',
                'data' => $group->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    public function destroy($id)
    {
        $request = $this->service->delete($id);

        session()->flash('success', [
            'success' => true,
            'message' => $request['message'],
        ]);

        return redirect()->route('group.index');
    }
}
