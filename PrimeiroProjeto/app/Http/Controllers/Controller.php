<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function homePage()
    {
        $variavel = "Home page do sisteme de gestão de investimentos";

        return view('Welcome', [
            'title' => $variavel
        ]);
    }

    public function cadastrar()
    {
        return "Tela de Cadastro";
    }

    public function fazerLogin(){

        return view('user.login');
    }
}
