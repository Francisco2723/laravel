@extends('templates.masterview') 
@section('conteudo-view')
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<h1 class="main-title float-left">Usuarios</h1>
					<ol class="breadcrumb float-right">
						<li class="breadcrumb-item">Usuarios</li>
						<li class="breadcrumb-item active">Cadastrar</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<!-- end row -->
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card mb-3 noradius">
					<div class="card-header">
						<h3><i class="fa fa-user"></i> Cadastrar Usuarios</h3>
						Casdastrar usuarios para o acesso ao sistema
					</div>
					<div class="card-body">
						{!! Form::open(['route' => 'user.store', 'method' => 'post']) !!}
						<div class="row">
							<div class="col-md-4 mb-3">
								<div class="form-group">
									<label for="exampleInputEmail1">Nome</label> {!! Form::text('name', '', array('class' => 'form-control noradius',
									'required' => 'true', 'placeholder' => 'Nome do Usuário')) !!} {{--
									<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required>--}}
								</div>
							</div>
							<div class="col-md-4 mb-3">
								<div class="form-group">
									<label for="exampleInputEmail1">CPF</label> {!! Form::text('cpf', '', array('class' => 'form-control noradius',
									'required' => 'true', 'placeholder' => 'Cpf do Usuário')) !!} {{--
									<input type="number" class="form-control" id="exampleInputNumber1" aria-describedby="numberlHelp" placeholder="Enter number"
									 required>--}}
								</div>
							</div>
							<div class="col-md-4 mb-3">
								<div class="form-group">
									<label>Telefone</label> {!! Form::text('phone', '', array('class' => 'form-control noradius', 'placeholder' => 'Telefone
									do Usuário')) !!} {{--}}
									<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>--}}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4 mb-3">
								<div class="form-group">
									<label>E-Mail</label> {!! Form::email('email', '', array('class' => 'form-control noradius', 'required' => 'true',
									'placeholder' => 'Email do Usuário')) !!} {{--}}
									<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>--}}
								</div>
							</div>
							<div class="col-md-4 mb-3">
								<div class="form-group">
									<label>Password</label> {!! Form::password('password', array('class' => 'form-control noradius', 'required' => 'true'))
									!!} {{--}}
									<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>--}}
								</div>
							</div>
						</div>
						{{-- <button type="submit" class="btn btn-primary">Submit</button>--}} {!! Form::submit('Cadastrar', array('class'
						=> 'btn btn-primary noradius')) !!} {!! Form::close() !!}
					</div>
				</div>
				<!-- end card-->
			</div>
		</div>
		@if(session('success'))
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card-body">
					<div class="alert alert-success" role="alert">
						<h4 class="alert-heading">Atenção</h4>
						<p> {{ session('success')['message'] }} </p>
					</div>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="card mb-3">
					<div class="card-header">
						<h3><i class="fa fa-table"></i> Lista de Usuários</h3>
						Lista de todos os usuários cadastrados
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="example1" class="table table-bordered table-hover display">
								<thead>
									<tr class="header-table">
										<th>Nome</th>
										<th>Cpf</th>
										<th>Telefone</th>
										<th>Email</th>
										<th>Nascimento</th>
										<th>Status</th>
										<th>Permissão</th>
										<th>Ação</th>
									</tr>
								</thead>
								<tbody>
									@foreach($users as $user)
									<tr>
										<td>{{$user->name}}</td>
										<td>{{$user->cpf}}</td>
										<td>{{$user->phone}}</td>
										<td>{{$user->email}}</td>
										<td>{{$user->birth}}</td>
										<td>{{$user->status}}</td>
										<td>{{$user->permission}}</td>
										<td>
											{!! Form::open(['route' => ['user.destroy', $user->id], 'method' => 'DELETE']) !!} {!! Form::submit('Excluir') !!} {!! Form::close()
											!!}
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- end card-->
			</div>
		</div>
	</div>
</div>
@endsection
 ()