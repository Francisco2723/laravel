@extends('templates.masterview') 
@section('conteudo-view')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <div class="breadcrumb-holder">
                  
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card mb-3 noradius">
                    <div class="card-header">
                        <h3><i class="fa fa-institution"></i> Cadastrar Insituição</h3>
                        Casdastrar instituições no sistema
                    </div>
                    <div class="card-body">
                        {!! Form::open(['route' => 'instituition.store', 'method' => 'post']) !!}
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nome</label> {!! Form::text('name', '', array('class'
                                    => 'form-control noradius', 'required' => 'true', 'placeholder' => 'Nome do Instituição'))
                                    !!}
                                </div>
                            </div>
                        </div>
                        {{-- <button type="submit" class="btn btn-primary">Submit</button>--}} {!! Form::submit('Cadastrar',
                        array('class' => 'btn btn-primary noradius')) !!} {!! Form::close() !!} @if(session('success'))
                        <div class="card-body">
                            <div class="alert alert-success" role="alert">
                                {{ session('success')['message'] }}
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                <!-- end card-->
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card mb-3">
                    <div class="card-header">
                        <h3><i class="fa fa-table"></i> Lista de Instituições</h3>
                        Lista de todos as instituições cadastradas
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-hover display">
                                <thead>
                                    <tr class="header-table">
                                        <th></th>
                                        <th>Nome da Instituição</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($instituitions as $instituition)
                                    <tr>
                                        <td>{{$instituition->id}}</td>
                                        <td>{{$instituition->name}}</td>
                                        <td>
                                            {!! Form::open(['route' => ['instituition.destroy', $instituition->id], 'method' => 'DELETE']) !!} {!! Form::submit('Excluir')
                                            !!} {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end card-->
            </div>
        </div>
    </div>
</div>
@endsection
 ()